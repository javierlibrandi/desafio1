package desafio.tecnico.supervielle.desafio1.dto;

public class Estadistica {

	private int cantidad_hombres;
	private int cantidad_mujeres;
	private int porcenteje_argentino;

	
	public Estadistica(int hombres, int mujeres, int porcenteje) {
		this.cantidad_hombres = hombres;
		this.cantidad_mujeres = mujeres;
		this.porcenteje_argentino = porcenteje;
	}

	/**
	 * @return the porcenteje_argentino
	 */
	public int getPorcenteje_argentino() {
		return porcenteje_argentino;
	}

	/**
	 * @param porcenteje_argentino the porcenteje_argentino to set
	 */
	public void setPorcenteje_argentino(int porcenteje_argentino) {
		this.porcenteje_argentino = porcenteje_argentino;
	}

	/**
	 * @return the cantidad_hombres
	 */
	public int getCantidad_hombres() {
		return cantidad_hombres;
	}

	/**
	 * @param cantidad_hombres the cantidad_hombres to set
	 */
	public void setCantidad_hombres(int cantidad_hombres) {
		this.cantidad_hombres = cantidad_hombres;
	}

	/**
	 * @return the cantidad_msujeres
	 */
	public int getCantidad_mujeres() {
		return cantidad_mujeres;
	}

	/**
	 * @param cantidad_msujeres the cantidad_msujeres to set
	 */
	public void setCantidad_mujeres(int cantidad_mujeres) {
		this.cantidad_mujeres = cantidad_mujeres;
	}



	

}
