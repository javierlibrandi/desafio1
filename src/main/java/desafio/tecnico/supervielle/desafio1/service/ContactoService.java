package desafio.tecnico.supervielle.desafio1.service;

import java.util.List;

import desafio.tecnico.supervielle.desafio1.dominio.DatoContacto;

public interface ContactoService {

	boolean existeContacto(List<DatoContacto> datosContactos);
	

}
