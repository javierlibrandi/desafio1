package desafio.tecnico.supervielle.desafio1.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import desafio.tecnico.supervielle.desafio1.dao.DatoContactoDAO;
import desafio.tecnico.supervielle.desafio1.dao.PersonaDAO;
import desafio.tecnico.supervielle.desafio1.dominio.DatoContacto;
import desafio.tecnico.supervielle.desafio1.dominio.Persona;
import desafio.tecnico.supervielle.desafio1.dominio.PersonaSexoType;
import desafio.tecnico.supervielle.desafio1.dto.Estadistica;

@Service
public class PersonaServiceImp implements PersonaService {
	final static Logger logger = LoggerFactory.getLogger(PersonaServiceImp.class);
	@Autowired
	private PersonaDAO personaDAO;

	@Autowired
	private DatoContactoDAO datoContactoDAO;



	@Transactional
	public Persona addPersona(Persona persona) {
		if(this.buscarPorIdentificador(persona).isPresent()) {
			return null;
		}
		persona.getDatosContactos().forEach((final DatoContacto datoContacto) -> datoContactoDAO.save(datoContacto));
		return personaDAO.save(persona);
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<Persona> findById(Long id) {
		Optional<Persona> persona = personaDAO.findById(id);
		return persona;
	}

	@Transactional(readOnly = true)
	public List<Persona> listarPersonas() {
		List<Persona> personas = new ArrayList<Persona>();
		for (Persona persona : personaDAO.findAll()) {
			personas.add(persona);
		}
		return personas;
	}

	@Override
	@Transactional(readOnly = true)
	public boolean existePersona(Persona persona) {

		Persona existingUser = personaDAO.findByNumeroAndTipoAndPaisAndSexo(persona.getNumero(), persona.getTipo(),
				persona.getPais(), persona.getSexo()).get();

		logger.debug("Persona {}", existingUser);

		if (existingUser == null) {
			return false;
		}
		return true;

	}

	@Override
	@Transactional
	public void borrarPersona(Persona persona) {
		this.findById(persona.getId()).get().getDatosContactos()
				.forEach((final DatoContacto datoContacto) -> datoContactoDAO.delete(datoContacto));
		personaDAO.delete(persona);

	}


	@Override
	@Transactional
	public Persona modificarPersona(Persona persona) {
		persona.getDatosContactos().forEach((final DatoContacto datoContacto) -> datoContactoDAO.save(datoContacto));
		return personaDAO.save(persona);


	}

	private Optional<Persona>  buscarPorIdentificador(Persona persona) {
		return personaDAO.findByNumeroAndTipoAndPaisAndSexo(persona.getNumero(), persona.getTipo(),
				persona.getPais(), persona.getSexo());

	}

	@Override
	public Estadistica getEstadisticas() {

		int hombres = personaDAO.countSexo(PersonaSexoType.Hombre.name());
		int mujeres = personaDAO.countSexo(PersonaSexoType.Mujer.name());
		logger.debug("CAntidad de hombress {}", hombres);
		logger.debug("CAntidad de mujeres {}", mujeres);

		Estadistica et = new Estadistica(hombres, mujeres, this.porcentejeArgentinos());
		return et;
	}

	private int porcentejeArgentinos() {
		long totRegistros = personaDAO.count();
		
		if (totRegistros == 0) {
			return 0;
		}
		
		int totArgentinos = personaDAO.cantNacionalidad("AR");

		return (int) (totArgentinos * 100 / totRegistros);
	}

	@Override
	@Transactional
	public Persona relacionarPadreHijo(Persona hijo, Persona padre) {
		Persona h = findById(hijo.getId()).get();

		if (h.getPadre() != null) {
			return h;
		}
		h.setPadre(findById(padre.getId()).get());
		this.modificarPersona(h);
		return h;
	}

	@Override
	@Transactional(readOnly = true)
	public String relacion(Long id1, Long id2) {
		Persona h1 = findById(id1).get();
		Persona h2 = findById(id2).get();
		// tienen el mismo padre
		if (h1.getPadre().equals(h2.getPadre())) {
			return "Hermanos@";
		}
		// tienen el mismo abuelo y distito padre
		if (this.primos(h1, h2)) {
			return "Primos@";
		}
		// para uno es el abuelo y para otro e padre
		if (this.tios(h1, h2)) {
			return "Tíos@";
		}

		if (this.tios(h2, h1)) {
			return "Tíos@";
		}

		return "sin relacion@";
	}

	private boolean primos(Persona h1, Persona h2) {
		Persona a1 = findById(h1.getPadre().getId()).get();
		Persona a2 = findById(h2.getPadre().getId()).get();

		if (a1.getPadre() == null || a2.getPadre() == null) {
			return false;
		}

		if (a1.getPadre().equals(a2.getPadre())) {
			return true;
		}
		return false;
	}

	private boolean tios(Persona h1, Persona h2) {

		Persona abuelo = findById(h1.getPadre().getId()).get();

		if (abuelo.getPadre() != null && h2.getPadre() != null && abuelo.getPadre().getId() == h2.getPadre().getId()) {
			return true;
		}
		return false;
	}



}