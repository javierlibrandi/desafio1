package desafio.tecnico.supervielle.desafio1.service;

import java.util.List;
import java.util.Optional;

import desafio.tecnico.supervielle.desafio1.dominio.Persona;
import desafio.tecnico.supervielle.desafio1.dto.Estadistica;

public interface PersonaService {

	Persona addPersona(Persona persona);

	List<Persona> listarPersonas();

	boolean existePersona(Persona persona);

	Optional<Persona> findById(Long id);

	void borrarPersona(Persona persona);

	Persona modificarPersona(Persona persona);

	Estadistica getEstadisticas();

	Persona relacionarPadreHijo(Persona hijo, Persona padre);

	String relacion(Long id1, Long id2);



}
