package desafio.tecnico.supervielle.desafio1.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import desafio.tecnico.supervielle.desafio1.dao.DatoContactoDAO;
import desafio.tecnico.supervielle.desafio1.dominio.DatoContacto;

@Service
public class ContactoServiceImp implements ContactoService {

	@Autowired
	private DatoContactoDAO datoContactoDAO;

	@Override
	public boolean existeContacto(List<DatoContacto> datosContacto) {

		datosContacto.forEach(datoContacto -> {
			 int cant = datoContactoDAO.existsDatoContactoCustomQuery(datoContacto.getNumeroTelefono(),datoContacto.getDatosContactoType()); 
			if (cant > 0) {
				return;
			}
		});
		return false;
		

	}

}
