package desafio.tecnico.supervielle.desafio1.rest;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import desafio.tecnico.supervielle.desafio1.dominio.Persona;
import desafio.tecnico.supervielle.desafio1.dto.Estadistica;
import desafio.tecnico.supervielle.desafio1.service.PersonaService;

@RestController
@RequestMapping("/api")
public class PersonaRestController {
	final static Logger logger = LoggerFactory.getLogger(PersonaRestController.class);

	@Autowired
	private PersonaService personaService;


	@GetMapping("/personas")
	public List<Persona> getAllPersonas() {
		logger.debug("Listo persona");
		return personaService.listarPersonas();
	}

	@DeleteMapping(value = "/persona/{id}")
	public @ResponseBody ResponseEntity<?> delete(@PathVariable("id") Long id) {
		  Optional<Persona> personanew = personaService.findById(id);

		  if (!personanew.isPresent()) {

			  return new ResponseEntity<String>("Persona " + id + " not found", HttpStatus.BAD_REQUEST);
	        }

		  return new ResponseEntity<String>("OK", HttpStatus.OK);

	}

	@PostMapping("/persona")
	public ResponseEntity<?> createPersona(@Valid @RequestBody Persona persona)
			throws Exception {
		logger.debug("Creo persona");

		Persona newPersona = personaService.addPersona(persona);
		if (newPersona==null) {
			return new ResponseEntity<String>("Persona " + persona.toString() + " ya se encuenta dada de alta",
					HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<Persona>(newPersona, HttpStatus.OK);
	}

	@PostMapping(value = "/persona/update")
	public @ResponseBody ResponseEntity<?> update(@Valid @RequestBody Persona persona) {

		Optional<Persona> personanew = personaService.findById(persona.getId());

		  if (!personanew.isPresent()) {

			  return new ResponseEntity<String>("Persona " + persona.getId() + " no existe", HttpStatus.BAD_REQUEST);
	        }

		  return new ResponseEntity<Persona>(personaService.modificarPersona(persona), HttpStatus.OK);


	}

	@GetMapping(value = "/persona/{id}")
	public @ResponseBody ResponseEntity<?> getPersona(@PathVariable("id") Long id)   {

		Optional<Persona> persona = personaService.findById(id);

		if (!persona.isPresent()) {

			  return new ResponseEntity<String>("Persona " + id + " no existe", HttpStatus.BAD_REQUEST);
	        }

		  return new ResponseEntity<Persona>(persona.get(), HttpStatus.OK);


	}


	/**
	 * Estadisticas
	 */
	@GetMapping("/estadisticas")
	public @ResponseBody ResponseEntity<?> getEstadisticas() {
		logger.info("Obtengo estadisticas");
		return new ResponseEntity<Estadistica>(personaService.getEstadisticas(), HttpStatus.OK);

	}

	/**
	 * Reaciones
	 */
	@PostMapping("/personas/{padre}/padre/{hijo}")
	public @ResponseBody ResponseEntity<?> update(@PathVariable("padre") Long padre, @PathVariable("hijo") Long hijo) {

		logger.info("Id del padre {} id del hijo {}", padre, hijo);

		Optional<Persona> ph = personaService.findById(hijo);
		Optional<Persona> pp = personaService.findById(padre);

		if (!ph.isPresent()) {
			return new ResponseEntity<String>("Hijo " + hijo + " not existe", HttpStatus.BAD_REQUEST);
		}

		if (!pp.isPresent()) {
			return new ResponseEntity<String>("Padre " + padre + " not existe", HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<Persona>(personaService.relacionarPadreHijo(ph.get(), pp.get()), HttpStatus.OK);
	}

	/**
	 * Reaciones
	 */
	@GetMapping("/relaciones/{id1}/{id2}")
	public @ResponseBody ResponseEntity<?> getRelacion(@PathVariable("id1") Long id1, @PathVariable("id2") Long id2) {

		logger.info("Id1 {} id2 {}", id1, id2);

		return new ResponseEntity<String>(personaService.relacion(id1, id2), HttpStatus.OK);
	}
}
