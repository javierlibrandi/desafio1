package desafio.tecnico.supervielle.desafio1.dominio;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class DatoContacto implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "contacto_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Enumerated(EnumType.STRING)
	private DatoContactoType datosContactoType;
	@Column(name="numero_telefono", nullable = false)
	private int numeroTelefono;


	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the datosContactoType
	 */
	public DatoContactoType getDatosContactoType() {
		return datosContactoType;
	}

	/**
	 * @param datosContactoType the datosContactoType to set
	 */
	public void setDatosContactoType(DatoContactoType datosContactoType) {
		this.datosContactoType = datosContactoType;
	}

	/**
	 * @return the numeroTelefono
	 */
	public int getNumeroTelefono() {
		return numeroTelefono;
	}

	/**
	 * @param numeroTelefono the numeroTelefono to set
	 */
	public void setNumeroTelefono(int numeroTelefono) {
		this.numeroTelefono = numeroTelefono;
	}

}
