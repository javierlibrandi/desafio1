package desafio.tecnico.supervielle.desafio1.dominio;

public enum DatoContactoType {
	PRINCIPAL, CELULAR, CASA, OTRO
}
