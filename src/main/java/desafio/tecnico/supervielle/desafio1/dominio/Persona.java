package desafio.tecnico.supervielle.desafio1.dominio;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity(name = "Persona")
@Table(uniqueConstraints = {
	      @UniqueConstraint(columnNames = {"DOC_NUMERO","DOC_TIPO","ID_PAIS","SEXO"}, name = "uniqueNameConstraint")},
indexes = { @Index(name = "PAIS_IX", columnList = "ID_PAIS", unique = false)})

public class Persona implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "persona_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;


//	---------------------------------------
	@ManyToOne(optional = false)
	@JoinColumn(name = "ID_PAIS")
	private Pais pais;


	@Enumerated(EnumType.STRING)
	@Column(name="SEXO", nullable = false)
	private PersonaSexoType sexo;

	@Enumerated(EnumType.STRING)
	@Column(name = "DOC_TIPO",nullable = false)
	private DocumentoType tipo;


	@Column(name = "DOC_NUMERO",nullable = false)
	private String numero;
//	---------------------------------------------

	/**
	 * @return the pais
	 */
	public Pais getPais() {
		return pais;
	}

	/**
	 * @param pais the pais to set
	 */
	public void setPais(Pais pais) {
		this.pais = pais;
	}

	/**
	 * @return the sexo
	 */
	public PersonaSexoType getSexo() {
		return sexo;
	}

	/**
	 * @param sexo the sexo to set
	 */
	public void setSexo(PersonaSexoType sexo) {
		this.sexo = sexo;
	}

	/**
	 * @return the tipo
	 */
	public DocumentoType getTipo() {
		return tipo;
	}

	/**
	 * @param tipo the tipo to set
	 */
	public void setTipo(DocumentoType tipo) {
		this.tipo = tipo;
	}

	/**
	 * @return the numero
	 */
	public String getNumero() {
		return numero;
	}

	/**
	 * @param numero the numero to set
	 */
	public void setNumero(String numero) {
		this.numero = numero;
	}

	@Column(name = "D_APELLIDOS", nullable = false, length = 100)
	private String apellido;

	@Column(name = "D_NOMBRES", nullable = false, length = 100)
	private String nombre;

	@NotNull(message = "La persona debe ser mayo a 18 años")
	@Min(18)
	@Column(nullable = false)
	private Integer edad;

	@NotEmpty(message = "Debe tener al menos un numero de contacto")
	@OneToMany(cascade = CascadeType.ALL)
	@Column(name = "datos_contato")
	private List<DatoContacto> datosContactos = new ArrayList<>();

	@ManyToOne
	private Persona Padre;

	/**
	 * @return the datosContactos
	 */
	public List<DatoContacto> getDatosContactos() {
		return datosContactos;
	}

//	/**
//	 * @param datosContactos the datosContactos to set
//	 */
	public void setDatosContactos(List<DatoContacto> datosContactos) {
		this.datosContactos = datosContactos;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}


	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}



	/**
	 * @return the edad
	 */
	public Integer getEdad() {
		return edad;
	}

	/**
	 * @param edad the edad to set
	 */
	public void setEdad(Integer edad) {
		this.edad = edad;
	}

	/**
	 * @return the padre
	 */
	public Persona getPadre() {
		return Padre;
	}

	/**
	 * @param padre the padre to set
	 */
	public void setPadre(Persona padre) {
		Padre = padre;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((numero == null) ? 0 : numero.hashCode());
		result = prime * result + ((pais == null) ? 0 : pais.hashCode());
		result = prime * result + ((sexo == null) ? 0 : sexo.hashCode());
		result = prime * result + ((tipo == null) ? 0 : tipo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Persona other = (Persona) obj;
		if (numero == null) {
			if (other.numero != null)
				return false;
		} else if (!numero.equals(other.numero))
			return false;
		if (pais == null) {
			if (other.pais != null)
				return false;
		} else if (!pais.equals(other.pais))
			return false;
		if (sexo != other.sexo)
			return false;
		if (tipo != other.tipo)
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Persona [");
		if (id != null) {
			builder.append("id=");
			builder.append(id);
			builder.append(", ");
		}
		if (pais != null) {
			builder.append("pais=");
			builder.append(pais);
			builder.append(", ");
		}
		if (sexo != null) {
			builder.append("sexo=");
			builder.append(sexo);
			builder.append(", ");
		}
		if (tipo != null) {
			builder.append("tipo=");
			builder.append(tipo);
			builder.append(", ");
		}
		if (numero != null) {
			builder.append("numero=");
			builder.append(numero);
			builder.append(", ");
		}
		if (apellido != null) {
			builder.append("apellido=");
			builder.append(apellido);
			builder.append(", ");
		}
		if (nombre != null) {
			builder.append("nombre=");
			builder.append(nombre);
			builder.append(", ");
		}
		if (edad != null) {
			builder.append("edad=");
			builder.append(edad);
			builder.append(", ");
		}
		if (datosContactos != null) {
			builder.append("datosContactos=");
			builder.append(datosContactos);
			builder.append(", ");
		}
		if (Padre != null) {
			builder.append("Padre=");
			builder.append(Padre);
		}
		builder.append("]");
		return builder.toString();
	}



}
