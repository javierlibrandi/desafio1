package desafio.tecnico.supervielle.desafio1.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import desafio.tecnico.supervielle.desafio1.dominio.DatoContacto;
import desafio.tecnico.supervielle.desafio1.dominio.DatoContactoType;

@Repository
public interface DatoContactoDAO extends CrudRepository<DatoContacto, Long> {

	@Query(value = "select count(1)  from dato_contacto c where c.numero_Telefono = ?1 and c.datos_contacto_type = ?2", nativeQuery = true)
	int existsDatoContactoCustomQuery(@Param("numero") int numero, @Param("tipo") DatoContactoType tipo);

}
