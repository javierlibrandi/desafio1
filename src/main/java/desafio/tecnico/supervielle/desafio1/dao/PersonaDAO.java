package desafio.tecnico.supervielle.desafio1.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import desafio.tecnico.supervielle.desafio1.dominio.DocumentoType;
import desafio.tecnico.supervielle.desafio1.dominio.Pais;
import desafio.tecnico.supervielle.desafio1.dominio.Persona;
import desafio.tecnico.supervielle.desafio1.dominio.PersonaSexoType;


public interface PersonaDAO extends CrudRepository<Persona, Long>{


	long count();

	@Query(value = "select count(1)  from persona p where p.id_pais = ?1" , nativeQuery = true)
	int cantNacionalidad(String string);

	@Query(value ="SELECT COUNT(*) FROM persona WHERE sexo = ?1", nativeQuery = true)
	int countSexo(String sexo);



	Optional<Persona> findByNumeroAndTipoAndPaisAndSexo(String numero, DocumentoType tipo, Pais pais, PersonaSexoType sexo);




}
